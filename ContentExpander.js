import React from 'react';
import PropTypes from 'prop-types';

//import Content from 'content';
import {Content, Editable, Heading, Img, RichText, Text} from 'editable';

import './ContentExpander.scss';

/**
 * ContentExpander
 * @description [description]
 * @example
  <div id="ContentExpander"></div>
  <script>
	ReactDOM.render(React.createElement(Components.ContentExpander, {
	}), document.getElementById("ContentExpander"));
  </script>
 */
class ContentExpander extends React.Component {
	constructor(props) {
		super(props);

		// config
		this.baseClass = 'content-expander';

		this.state = {
			open: this.props.isOpen,
			height: 'auto'
		};
	}

	onResize = () => {
		this.resize();
	};

	open() {
		this.setState({
			open: true,
			height: this.openHeight
		});
	}

	close() {
		this.setState({
			open: false,
			height: 0
		});
	}

	toggle() {
		if (this.state.open) {
			this.close();
		} else {
			this.open();
		}

		const {onToggle} = this.props;

		if (onToggle) {
			onToggle(this);
		}
	}

	resize() {
		if (this && this.expander) {
			this.setState({
				height: 'auto'
			});

			this.openHeight = this.expander.clientHeight;

			if (this.state.open) {
				this.setState({
					height: this.openHeight
				});
			} else {
				this.setState({
					height: 0
				});
			}
		}
	}

	componentWillReceiveProps(props) {
		if (!props.isOpen) {
			this.setState({
				open: false,
				height: 0
			});
		} else {
			this.setState({
				open: true,
				height: this.openHeight
			});
		}
	}

	componentDidMount() {
		this.openHeight = this.expander.clientHeight;

		if (this.state.open) {
			this.setState({
				height: this.openHeight
			});
		} else {
			this.setState({
				height: 0
			});
		}

		if (!SERVER) {
			if (typeof window === 'object') {
				window.addEventListener('resize', this.onResize);
			}
		}
	}

	componentWillUnmount() {
		if (!SERVER) {
			if (typeof window === 'object') {
				window.removeEventListener('resize', this.onResize);
			}
		}
	}

	render() {
		const {children, className} = this.props;
		const {open} = this.state;

		const content = typeof children === 'string' ? <Content content={children} /> : children;

		return (
			<div
				className={`${this.baseClass} ${className} ${open ? 'open' : 'closed'}`}
				ref={expander => (this.expander = expander)}
				style={{height: this.state.height}}
			>
				{content}
			</div>
		);
	}
}

ContentExpander.defaultProps = {
	className: '',
	isOpen: true,
	children: '',
	onToggle: null
};

ContentExpander.propTypes = {
	className: PropTypes.string,
	isOpen: PropTypes.bool,
	children: PropTypes.oneOfType([PropTypes.array, PropTypes.string, PropTypes.object]),
	onToggle: PropTypes.func
};

export default ContentExpander;
